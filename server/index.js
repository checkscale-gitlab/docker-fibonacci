const keys = require('./keys')

// Express setup

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()
app.use(cors())
app.use(bodyParser.json())

//  Postgress setup

const {Pool} = require('pg')

const pgClient = new Pool({
    user: keys.pgUser,
    host: keys.pgHost,
    database: keys.pgDatabase,
    password: keys.pgPassqord,
    port: keys.pgPort
})
pgClient
    .on('error', () => console.log('Lost PG connection')
)

pgClient
    .query('CREATE TABLE IF NOT EXISTS values (number INT')
    .catch((err) => console.log(err))

//  Redis setup
const redi = require('redis')
const redisClient = redis.creatClient({
    host: keys.redisHost,
    port: keys.redisPort,
    retry_strategy: () => 1000
})

const redisPublisher = redisClient.duplicate()

// endpoints

app.get('/', (reg, res) => {
    res.send('Hi')
})


app.get('/values/all', async (reg, res) => {
    const values = await pgClient.query('SELECT * FROM values')

    res.send(values.rows)
})

app.get('values/current', async (req, res) => {
    redisClient.hgetall('values', (err, values) => {
        res.send(values)
    })
})

app.post('/values', async (reg, res) => {
    const index = req.body.index
    if(parseInt(index) > 40) {
        return res.status(442).send('Index too high')
    }

    redisClient.hset('values', index, 'nothing yet!')
    redisPublisher.publish('insert', index)
    pqClient.query('INSERT INTO values(number) VALUES($1)', [index])

    res.send({working: true})

})

app.listen(5000, err => {
    console.log('Listening')
})
